<?php

namespace Cyrille37\PhpCsvReader;

class Row implements \ArrayAccess, \Countable
{
    protected $row;
    protected $headers;

    public function __construct(array &$headers = null, array &$row)
    {
        $this->headers = $headers;
        $this->row = $row;
    }

    public function getHeaders(): Array
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->row;
    }

    /**
     * @inheritdoc
     */
    public function count(): int
    {
        return count($this->row);
    }

    /**
     * @throws \RuntimeException not implemented
     * @inheritdoc
     */
    public function offsetSet($offset, $value): void
    {
        throw new \RuntimeException('ArrayAccess::offsetSet is not implemented');
    }

    /**
     * @throws \RuntimeException not implemented
     * @inheritdoc
     */
    public function offsetUnset($offset): void
    {
        throw new \RuntimeException('ArrayAccess::offsetSet is not implemented');
    }

    /**
     * $column is insensitive case.
     * @inheritdoc
     * @throws \InvalidArgumentException if Headers not set.
     */
    public function offsetExists($column): bool
    {
        if (!$this->headers)
            throw new \InvalidArgumentException('Headers not set');
        return isset($this->headers[\mb_strtolower($column)]);
    }

    /**
     * $column is insensitive case.
     * 
     * @inheritdoc
     * @throws \InvalidArgumentException if Headers not set.
     * @throws \InvalidArgumentException if unknown column.
     */
    public function offsetGet($column)
    {
        if (!$this->headers)
            throw new \InvalidArgumentException('Headers not set');
        $column = \mb_strtolower($column);
        if (!isset($this->headers[$column]))
            throw new \InvalidArgumentException('Unknow column "' . $column . '"');
        return $this->row[$this->headers[$column]];
    }
}
