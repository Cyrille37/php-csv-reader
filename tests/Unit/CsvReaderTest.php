<?php

namespace Tests\Unit;

use Cyrille37\PhpCsvReader\Reader as CsvReader;
use PHPUnit\Framework\TestCase;

class CsvReaderTest extends TestCase
{
    const FILE_1 = __DIR__ . '/files/CsvReaderTest_01.csv';
    const SEPARATOR = ';' ;
    const CSV_DATA = [
        ['Prénom' => 'Jémila', 'Nom' => 'Clessou-Laporte', 'Âge' => 0],
        ['Prénom' => 'Justin', 'Nom' => 'Ptitpipi', 'Âge' => 1],
        ['Prénom' => 'Candice', 'Nom' => 'Paray-Lejour', 'Âge' => 2],
    ];

    public function testReadOkAll()
    {
        $csv = new CsvReader(self::FILE_1, self::SEPARATOR );

        $i = 0;
        foreach ($csv->getRows() as $row) {
            $this->assertArrayHasKey('Prénom', $row);
            $this->assertEquals(self::CSV_DATA[$i]['Prénom'], $row['Prénom']);
            $this->assertArrayHasKey('Nom', $row);
            $this->assertEquals(self::CSV_DATA[$i]['Nom'], $row['Nom']);
            $this->assertArrayHasKey('Âge', $row);
            $this->assertEquals(self::CSV_DATA[$i]['Âge'], $row['Âge']);
            $i++;
        }
        $this->assertEquals(3, $i);
    }

    public function testColumnHeaderCase()
    {
        $csv = new CsvReader(self::FILE_1, self::SEPARATOR );

        $rows = $csv->getRows();
        $row = $rows->current();
        $this->assertArrayHasKey('Prénom', $row);
        $this->assertArrayHasKey('prénom', $row);
        $this->assertArrayHasKey('âge', $row);
    }

    public function testColumnsCount()
    {
        $csv = new CsvReader(self::FILE_1, self::SEPARATOR );

        $rows = $csv->getRows();
        $row = $rows->current();
        $this->assertEquals(4, count($row));
    }

    public function testHeaders()
    {
        $csv = new CsvReader(self::FILE_1, self::SEPARATOR );

        $headers = $csv->getHeaders();
        $this->assertEquals(4, count($headers));
    }

    public function testfindMissingHeaders()
    {
        $csv = new CsvReader(self::FILE_1, self::SEPARATOR );

        $missing = $csv->findMissingHeaders( ['Nom', 'Prénom', 'Ville']);
        $this->assertEquals(1, count($missing));
        $this->assertEquals('Ville', $missing[0]);

        // Must test if getRows() works well (file must be rewinded in Reader).
        $i = 0;
        foreach ($csv->getRows() as $row) {
            $this->assertArrayHasKey('Prénom', $row);
            $this->assertEquals(self::CSV_DATA[$i]['Prénom'], $row['Prénom']);
            $this->assertArrayHasKey('Nom', $row);
            $this->assertEquals(self::CSV_DATA[$i]['Nom'], $row['Nom']);
            $this->assertArrayHasKey('Âge', $row);
            $this->assertEquals(self::CSV_DATA[$i]['Âge'], $row['Âge']);
            $i++;
        }
        $this->assertEquals(3, $i);
    }

}
