<?php

namespace Cyrille37\PhpCsvReader;

use SplFileObject;
use NoRewindIterator;

/**
 * Can read large file because use an Iterator with Generator.
 */
class Reader
{
    protected $file;
    protected $headers = null;

    protected $separator ;
    protected $enclosure ;
    protected $escape ;

    /**
     * @throws \RuntimeException if $filename not exists.
     */
    public function __construct($filename, string $separator = ',', string $enclosure = '"', string $escape = '\\')
    {
        if (!file_exists($filename)) {
            throw new \RuntimeException('Not found "' . $filename . '"');
        }
        $this->separator = $separator ;
        $this->enclosure = $enclosure ;
        $this->escape = $escape ;
        $this->file = new SplFileObject($filename, 'r');
    }

    public function getHeaders(): array
    {
        if (!$this->headers)
            $this->getRows();
        return $this->headers;
    }

    public function & findMissingHeaders( array $wantedHeaders )
    {
        $headers = $this->getHeaders();
        $missing = [];
        foreach( $wantedHeaders as $h )
        {
            if( ! isset($headers[\mb_strtolower($h)]) )
                $missing[] = $h ;
        }
        return $missing ;
    }

    /**
     * @return \NoRewindIterator
     */
    public function getRows(): NoRewindIterator
    {
        $iterator = new NoRewindIterator($this->csvFileIterator());
        // start iterator and pull out header row as $rawHeaders
        if( ! $this->headers )
        {
            $rawHeaders = $iterator->current();
            if( ! $rawHeaders )
            {
                throw new \Exception('Could not get file headers, check that the CSV separator is the right one.');
            }
            $this->headers = [];
            $idx = 0;
            foreach ($rawHeaders->getRow() as $h) {
                $this->headers[mb_strtolower(trim($h))] = $idx ++;
            }
            // move pointer to the data rows
            $iterator->next();
        }
        else
        {
            // Must rewind to first line of data
            // Mandatory because of possible previous "getHeaders()" call(s),
            // and possible multiple call of "getRows()".
            $this->file->rewind();
            $row = $this->file->fgetcsv($this->separator, $this->enclosure, $this->escape);
        }
        return $iterator;
    }

    protected function csvFileIterator(): iterable
    {
        $count = 0;
        while (!$this->file->eof()) {
            $row = $this->file->fgetcsv($this->separator, $this->enclosure, $this->escape);
            if ($row === false || count($row) == 1)
                break;
            yield new Row($this->headers, $row);
            $count++;
        }
        return $count;
    }
}
