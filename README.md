# Php Csv Reader

Simple CSV Reader for Php.

Can read large file without consuming memory because it uses an iterator with a [Generator](https://www.php.net/manual/en/class.generator) that only read one line at a time.

Give access to columns by their "name".

## Install

```bash
composer require cyrille37/php-csv-reader
```

## Usage

```php
$csv = new CsvReader( 'foo.csv' );
foreach( $csv->getRows() as $row )
{
    echo $row['some column name'];
}
```

```php
$csv_separator = ';' ;
$csv = new CsvReader( 'foo.csv', $csv_separator );
```
